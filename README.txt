Introduction
------------

This module has an tutorial on YouTube: https://youtu.be/-Rw9ICBZK4w

That's your best route if you are unfamiliar with Rules.

This module implements two Rules actions.
1) Send push notification to flagging users of a node.
1) Send push notification to flagging users of a user.

Usage
------

Rules action 1: Send push notification to flagging users of a node.
 
This action requires the following context variables:

1) Node entity. (You may need to fetch entity by ID and pass that in)
2) Flag ID. This is the machine name of the flag you're focused on.
3) Notification title.
4) Message.
5) Content link. If a user clicks the push notification, they get sent here.

Rules action 2: Send push notification to flagging users of a user.
 
This action requires the following context variables:

1) User ID. This is the user ID who has been flagged.
2) Flag ID. This is the machine name of the flag you're focused on.
3) Notification title.
4) Message.
5) Content link. If a user clicks the push notification, they get sent here.
