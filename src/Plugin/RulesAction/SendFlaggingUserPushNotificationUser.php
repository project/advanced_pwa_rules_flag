<?php

namespace Drupal\advanced_pwa_rules_flag\Plugin\RulesAction;

use Drupal\rules\Core\RulesActionBase;
use Drupal\Core\Queue\QueueFactory;
use Drupal\advanced_pwa\Model\SubscriptionsDatastorage;
use Drupal\Component\Serialization\Json;

/**
 * Provides a 'Send push notification to flagging users of a user' action.
 *
 * @RulesAction(
 *   id = "advanced_pwa_rules_flag_user_notify",
 *   label = @Translation("Send Push Notification to flagging users of a user"),
 *   category = @Translation("Advanced PWA Rules Flag"),
 *   context_definitions = {
 *     "uid" = @ContextDefinition("integer",
 *       label = @Translation("User ID"),
 *       description = @Translation("Specifies the user id to check for flagging users")
 *     ),
 *     "flag_id" = @ContextDefinition("string",
 *       label = @Translation("Flag ID"),
 *       description = @Translation("Enter the ID/Machine name of the flag to check.")
 *     ),
 *     "notification_title" = @ContextDefinition("text",
 *       label = @Translation("Notification title"),
 *       description = @Translation("Enter the notification title.")
 *     ),
 *     "notification_message" = @ContextDefinition("string",
 *       label = @Translation("Message"),
 *       description = @Translation("Enter the notification content.")
 *     ),
 *     "content_link" = @ContextDefinition("string",
 *       label = @Translation("Content link"),
 *       description = @Translation("Enter the content link path to use for the notification. When users click the notification, this is where they go.")
 *     ),
 *   }
 * )
 */

class SendFlaggingUserPushNotificationUser extends RulesActionBase {

  /**
   * Sends the notification.
   */
  protected function doExecute($uid, $flag_id, $notification_title, $notification_message, $content_link) {
    
    // Check if push notifications are turned on globally
    $status = \Drupal::config('advanced_pwa.settings')->get('status.all');
    if ($status) {
        // Get flagging users of the user
        $user = \Drupal\user\Entity\User::load($uid);
        $flag_service = \Drupal::service('flag');
        $flag = $flag_service->getFlagById($flag_id);
        $flagging_users = $flag_service->getFlaggingUsers($user, $flag);
        
        // Get the push notification keys
        $advanced_pwa_config = \Drupal::config('advanced_pwa.advanced_pwa');
        $advanced_pwa_public_key = $advanced_pwa_config->get('public_key');
        $advanced_pwa_private_key = $advanced_pwa_config->get('private_key');
        
        // If there are flagging users and we have everything
        if (!empty($flagging_users) && !empty($advanced_pwa_public_key) && !empty($advanced_pwa_private_key)) {
          
        // Build the notification array
        $icon = $advanced_pwa_config->get('icon_path');
        $icon_path = \Drupal::service('file_url_generator')->generateAbsoluteString($icon);
        $entry = [
          'title' => $notification_title,
          'message' => $notification_message,
          'icon' => $icon_path,
          'url' => $content_link,
        ];
        $notification_data = Json::encode($entry);

        // Start the queue  
        /** @var QueueFactory $queue_factory */
        $queue_factory = \Drupal::service('queue');
        $queue = $queue_factory->get('cron_send_notification');
        $item = new \stdClass();
        $item->notification_data = $notification_data;

        $current_uid = \Drupal::currentUser()->id();
          foreach ($flagging_users as $flagging_user) { 
              $flagging_uid = $flagging_user->id();
              // Load all the devices for the flagging user
              $subscriptions = SubscriptionsDatastorage::loadAllByUID($flagging_uid);
              if (!empty($subscriptions)) {
                  // Send the flagging users devices push notifications
                  $item->subscriptions = $subscriptions;
                  $queue->createItem($item);
              }
          }
      }
    }   
  }
}
